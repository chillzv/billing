#!/bin/bash
psql -v ON_ERROR_STOP=1 --username "$DB_USER" -d "$DB_NAME"  <<-EOSQL
     create schema if not exists public;
     CREATE TABLE IF NOT EXISTS public.users
      (
          "id"   SERIAL,
          "name" varchar(255) NOT NULL,
          PRIMARY KEY ("id")
      );
      CREATE TABLE IF NOT EXISTS public.wallets
      (
          "id"         SERIAL,
          "user_id"    INT       NOT NULL REFERENCES users ("id") ON UPDATE CASCADE,
          "amount"     INT       NOT NULL CHECK (amount >= 0),
          "created_at" TIMESTAMP NOT NULL,
          "updated_at" TIMESTAMP NOT NULL,
          PRIMARY KEY ("id")
      );

      CREATE TABLE IF NOT EXISTS public.transactions
      (
          "id"              SERIAL,
          "sender"          INT       NOT NULL REFERENCES wallets ("id") ON UPDATE CASCADE,
          "receiver"        INT       NOT NULL REFERENCES wallets ("id") ON UPDATE CASCADE,
          "withdraw_amount" INT       NOT NULL CHECK (withdraw_amount > 0),
          "deposit_amount"  INT       NOT NULL CHECK (deposit_amount > 0),
          "tax"             INT       NOT NULL CHECK (tax > 0),
          "updated_at"      TIMESTAMP NOT NULL,
          "created_at"      TIMESTAMP NOT NULL,
          PRIMARY KEY ("id")
      );
      INSERT INTO public.users (id, name) VALUES (1, 'user1');
      INSERT INTO public.users (id, name) VALUES (2, 'user2');
      INSERT INTO public.wallets (id, user_id, amount, created_at, updated_at) VALUES (1, 1, 10000, '2019-09-09 05:35:33.902570', '2020-12-10 15:09:03.293004');
      INSERT INTO public.wallets (id, user_id, amount, created_at, updated_at) VALUES (2, 2, 10000, '2019-09-09 05:35:33.902570', '2020-12-10 15:09:03.293004');

EOSQL
