module billing

go 1.14

require (
	github.com/ardanlabs/conf v1.3.3
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dimfeld/httptreemux/v5 v5.2.2
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.9.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/errors v0.9.1
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.14.0
	go.opentelemetry.io/otel v0.14.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
