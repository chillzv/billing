package transaction

import "time"

type TransactionCreate struct {
	Id             uint64
	Sender         uint64 `json:"sender"`
	Receiver       uint64 `json:"receiver"`
	WithdrawAmount uint64 `json:"withdraw_amount"`
	DepositAmount  uint64 `json:"deposit_amount"`
	Tax            uint64
	CreatedAt      *time.Time
	UpdatedAt      *time.Time
}
