// Package user contains user related CRUD functionality.
package transaction

import (
	"context"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

var (
	// ErrNotFound is used when a specific Transaction is requested but does not exist.
	ErrNotFound = errors.New("not found")

	// ErrInvalidID occurs when an ID is not in a valid form.
	ErrInvalidID = errors.New("ID is not in its proper form")
)

// Transaction manages the set of API's for user access.
type Transaction struct {
	log *log.Logger
	db  *sqlx.DB
}

// New constructs a Transaction for api access.
func New(log *log.Logger, db *sqlx.DB) Transaction {
	return Transaction{
		log: log,
		db:  db,
	}
}

// Create transfer from one wallet to another
func (t Transaction) Create(ctx context.Context, traceID string, tc TransactionCreate, now time.Time) (id uint64, err error) {
	/*ctx, span := trace.SpanFromContext(ctx).Tracer().Start(ctx, "business.data.transaction.create")
	defer span.End()*/

	tx, err := t.db.BeginTxx(ctx, nil)
	if err != nil {
		return 0, errors.Wrap(err, "create tx error")
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	if err = t.withdrawFromSender(ctx, tx, tc, now); err != nil {
		return
	}
	if err = t.depositToReceiver(ctx, tx, tc, now); err != nil {
		return
	}
	id, err = t.createTransaction(ctx, tx, tc)
	if err != nil {
		return
	}

	return
}

func (t Transaction) createTransaction(ctx context.Context, tx *sqlx.Tx, tc TransactionCreate) (uint64, error) {
	const q = `insert into transactions 
				(sender, receiver, withdraw_amount, deposit_amount, tax, created_at, updated_at)
				values ($1,$2,$3,$4,$5,$6,$7) returning id;`
	_, err := tx.ExecContext(ctx, q, tc.Sender, tc.Receiver, tc.WithdrawAmount, tc.DepositAmount, tc.Tax, tc.CreatedAt, tc.UpdatedAt)
	if err != nil {
		return 0, errors.WithStack(err)
	}
	return 0, nil // todo return tr id
}

func (t Transaction) withdrawFromSender(ctx context.Context, tx *sqlx.Tx, tc TransactionCreate, now time.Time) error {

	const q = `	UPDATE wallets
				SET amount = amount - $1,
    			updated_at = $2
				WHERE user_id = $3;`

	res, err := tx.ExecContext(ctx, q, tc.WithdrawAmount, now, tc.Sender)
	// todo check for error (new row for relation "wallets" violates check constraint "wallets_amount_check")
	if err != nil {
		return errors.WithStack(err)
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return errors.WithStack(err)
	}
	if rows != 1 {
		return errors.Wrap(ErrNotFound, "sender wallet not found")
	}
	return nil
}

func (t Transaction) depositToReceiver(ctx context.Context, tx *sqlx.Tx, tc TransactionCreate, now time.Time) error {

	const q = `	UPDATE wallets
				SET amount = amount + $1,
    			updated_at = $2
				WHERE user_id = $3;`

	res, err := tx.ExecContext(ctx, q, tc.DepositAmount, now, tc.Receiver)
	// todo check for error (new row for relation "wallets" violates check constraint "wallets_amount_check")
	if err != nil {
		return errors.WithStack(err)
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return errors.WithStack(err)
	}
	if rows != 1 {
		return errors.Wrap(ErrNotFound, "receiver wallet not found")
	}
	return nil
}
