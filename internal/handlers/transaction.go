package handlers

import (
	"context"
	"math"
	"net/http"

	"billing/internal/business/data/transaction"
	"billing/internal/foundation/web"
	"github.com/pkg/errors"
)

type transactionGroup struct {
	transaction transaction.Transaction
	auth        interface{}
}

type CreateTransactionRequest struct {
	Sender   uint64 `json:"sender"`
	Receiver uint64 `json:"receiver"`
	Amount   uint64 `json:"amount"`
}

type CreateTransactionResponse struct {
	TransactionID uint64 `json:"transaction_id"`
}

const (
	taxPercent float64 = 1.5 // todo move to config or db
	minAmount uint64 = 100
)

func (tg transactionGroup) createTransaction(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	/*ctx, span := trace.SpanFromContext(ctx).Tracer().Start(ctx, "handlers.transactionGroup.update")
	defer span.End()*/

	v, ok := ctx.Value(web.KeyValues).(*web.Values)
	if !ok {
		return web.NewShutdownError("web value missing from context")
	}

	var cr CreateTransactionRequest
	if err := web.Decode(r, &cr); err != nil {
		return errors.Wrapf(err, "unable to decode payload")
	}
	// todo validation with lib
	if cr.Amount < minAmount {
		return web.NewRequestError(errors.New("invalid amount"), http.StatusBadRequest)
	}

	depositAmountFloat := float64(cr.Amount) - float64(cr.Amount)/float64(100) * taxPercent
	depositAmount := uint64(math.Round(depositAmountFloat))
	tr := transaction.TransactionCreate{
		Sender:         cr.Sender,
		Receiver:       cr.Receiver,
		WithdrawAmount: cr.Amount,
		DepositAmount:  depositAmount,
		Tax:            cr.Amount - depositAmount,
		CreatedAt:      &v.Now,
		UpdatedAt:      &v.Now,
	}

	transactionID, err := tg.transaction.Create(ctx, v.TraceID, tr, v.Now)
	if err != nil {
		switch err {
		case transaction.ErrNotFound:
			return web.NewRequestError(err, http.StatusNotFound)
		default:
			return errors.Wrapf(err, "ID:  Transaction: %+v", &cr)
		}
	}

	return web.Respond(ctx, w, CreateTransactionResponse{TransactionID: transactionID}, http.StatusNoContent)
}
